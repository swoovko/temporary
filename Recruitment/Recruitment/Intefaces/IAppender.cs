﻿using Recruitment.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment.Intefaces
{
	public interface IAppender
	{
		bool Initialize();

		void Log(string message, LogLevel logLevel);
	}
}
