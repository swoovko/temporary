﻿using Recruitment.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment.Intefaces
{
	public interface ILogger
	{
		void LogMessage(string message, LogLevel logLevel);
	}
}
