﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment.Intefaces
{
	public interface IConfigRepository
	{
		string GetConnectionString();

		string GetLogFilePath();
	}
}
