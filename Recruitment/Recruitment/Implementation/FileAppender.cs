﻿using Recruitment.Intefaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment.Implementation
{
	public class FileAppender : IAppender
	{
		private string logFilePath;
		private const string defaultFilename = "LogFile{0}.txt";
		private const string defaultDateFormat = "YYYY-MM-dd";
		private const string defaultTimeFormat = "HH-mm-ss";

		protected readonly IFileRepository fileRepository;
		protected readonly IConfigRepository configRepository;

		public FileAppender(IConfigRepository configRepository, IFileRepository fileRepository)
		{
			this.fileRepository = fileRepository;
			this.configRepository = configRepository;
		}

		public bool Initialize()
		{
			this.logFilePath = this.configRepository.GetLogFilePath();

			return true;
		}

		public void Log(string message, LogLevel logLevel)
		{
			var fullPath = Path.Combine(logFilePath, string.Format(defaultFilename, DateTime.Today.ToString(defaultDateFormat)));
			var formattedMessage = $"{Enum.GetName(typeof(LogLevel), logLevel)} {DateTime.Now.ToString(defaultTimeFormat)} {message}{Environment.NewLine}";

			this.fileRepository.AppendAllText(fullPath, formattedMessage);
		}
	}
}
