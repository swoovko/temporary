﻿using Recruitment.Intefaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment.Implementation
{
	public class ConfigRepository : IConfigRepository
	{
		private const string connectionString = "ConnectionString";
		private const string logFileDirectory = "LogFileDirectory";

		public string GetConnectionString()
		{
			return GetValue(connectionString);
		}

		public string GetLogFilePath()
		{
			return GetValue(logFileDirectory);
		}

		private string GetValue(string appSetting)
		{
			var value = ConfigurationManager.AppSettings[appSetting];
			if (string.IsNullOrWhiteSpace(value))
			{
				throw new ArgumentException($"{appSetting} is not set in app.config!");
			}
			return value;
		}
	}
}
