﻿using Recruitment.Intefaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment.Implementation
{
	public class FileRepository : IFileRepository
	{
		private static object _locker = new object();

		public void AppendAllText(string path, string contents)
		{
			lock (_locker)
			{
				File.AppendAllText(path, contents);
			}
		}
	}
}
