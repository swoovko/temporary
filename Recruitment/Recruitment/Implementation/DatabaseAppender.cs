﻿using Recruitment.Intefaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment.Implementation
{
	public class DatabaseAppender : IAppender
	{
		protected readonly IConfigRepository configRepository;

		private string connectionString;
		private const string query = "Insert into Log Values(@message, @level)";

		public DatabaseAppender(IConfigRepository configRepository)
		{
			this.configRepository = configRepository;
		}

		public bool Initialize()
		{
			this.connectionString = this.configRepository.GetConnectionString();

			return true;
		}

		public void Log(string message, LogLevel logLevel)
		{
			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				var command = new SqlCommand(query);
				command.Parameters.AddWithValue("message", message);
				command.Parameters.AddWithValue("level", (short)logLevel);
				command.ExecuteNonQuery();
			}
		}
	}
}
