﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment.Implementation
{
	public enum LogLevel : short
	{
		Message = 1,
		Warning = 2,
		Error = 3
	}
}
