﻿using Recruitment.Intefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment.Implementation
{
	public class JobLogger : ILogger
	{
		private readonly LogLevel minimumLogLevel;
		private readonly List<IAppender> appenders;

		public JobLogger(IEnumerable<IAppender> appenders, LogLevel minimumLogLevel)
		{
			this.minimumLogLevel = minimumLogLevel;

			this.appenders = new List<IAppender>();
			this.Initialize(appenders);
		}

		private void Initialize(IEnumerable<IAppender> appendersToAdd)
		{
			foreach (var appender in appendersToAdd)
			{
				try
				{
					if (appender.Initialize())
					{
						this.appenders.Add(appender);
					}
				}
				catch (Exception exc)
				{
					Console.WriteLine($"Cannot initialize appender {appender.GetType().ToString()}, exception: {exc.Message}!");
				}
			}
			if (this.appenders == null || this.appenders.Any() == false)
			{
				throw new ArgumentException("No suitable appender has been configured!");
			}
		}

		public void LogMessage(string message, LogLevel logLevel)
		{
			if (string.IsNullOrWhiteSpace(message))
			{
				return;
			}

			if (logLevel < minimumLogLevel)
			{
				return;
			}

			foreach (var appender in appenders)
			{
				try
				{
					// TODO: run appenders simultaneously in seperate tasks, and wait for all..
					appender.Log(message, logLevel);
				}
				catch (Exception exc)
				{
					Console.WriteLine($"Cannot write log entry. Appender: {appender.GetType()}. Exception: {exc.Message}.");
				}
			}
		}
	}
}
