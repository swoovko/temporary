﻿using Recruitment.Intefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment.Implementation
{
	public class ConsoleAppender : IAppender
	{
		private const string defaultTimeFormat = "HH:mm:ss";

		public bool Initialize()
		{
			return true;
		}

		public void Log(string message, LogLevel logLevel)
		{
			var beforeColor = Console.ForegroundColor;
			Console.ForegroundColor = this.GetColor(logLevel);
			Console.WriteLine($"{DateTime.Now.ToString(defaultTimeFormat)} {message}");
			Console.ForegroundColor = beforeColor;
		}

		private ConsoleColor GetColor(LogLevel level)
		{
			switch (level)
			{
				case LogLevel.Message:
					return ConsoleColor.White;
				case LogLevel.Warning:
					return ConsoleColor.Yellow;
				case LogLevel.Error:
					return ConsoleColor.Red;
				default:
					return ConsoleColor.Gray;
			}
		}
	}
}
