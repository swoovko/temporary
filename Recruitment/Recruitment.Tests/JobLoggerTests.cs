using Moq;
using NUnit.Framework;
using Recruitment.Implementation;
using Recruitment.Intefaces;
using System;
using System.Collections.Generic;

namespace Recruitment.Tests
{
	public class JobLoggerTests
	{
		[SetUp]
		public void Setup()
		{
		}

		[Test]
		public void Constructor_EmptyAppenderList_ShouldThrowException()
		{
			Assert.Throws<ArgumentException>(() => new JobLogger(new List<IAppender> { }, LogLevel.Message));
		}

		[TestCase("Test message1", LogLevel.Message)]
		[TestCase("Test message2", LogLevel.Warning)]
		[TestCase("Test message3", LogLevel.Error)]
		public void LogMessage_ConfiguredForAllLevels_ShouldLogAllLevels(string message, LogLevel logLevel)
		{
			var appenderMock = new Moq.Mock<IAppender>();
			appenderMock.Setup(a => a.Initialize()).Returns(true);

			var logger = new JobLogger(new List<IAppender> { appenderMock.Object }, LogLevel.Message);
			logger.LogMessage(message, logLevel);

			appenderMock.Verify(a => a.Log(message, logLevel), Times.Once);
		}

		[Test]
		public void LogMessage_ConfiguredForWarning_ShouldNotLogMessageLevel()
		{
			var appenderMock = new Moq.Mock<IAppender>();
			appenderMock.Setup(a => a.Initialize()).Returns(true);

			var logger = new JobLogger(new List<IAppender> { appenderMock.Object }, LogLevel.Warning);

			var msg = "Test message";
			var msgLevel = LogLevel.Message;
			logger.LogMessage(msg, msgLevel);

			appenderMock.Verify(a => a.Log(msg, msgLevel), Times.Never);
		}

		[Test]
		public void LogMessage_ConfiguredForWarning_ShouldLogWarningLevel()
		{
			var appenderMock = new Moq.Mock<IAppender>();
			appenderMock.Setup(a => a.Initialize()).Returns(true);

			var logger = new JobLogger(new List<IAppender> { appenderMock.Object }, LogLevel.Warning);

			var msg = "Test message";
			var msgLevel = LogLevel.Warning;
			logger.LogMessage(msg, msgLevel);

			appenderMock.Verify(a => a.Log(msg, msgLevel), Times.Once);
		}

		[Test]
		public void LogMessage_ConfiguredForWarning_ShouldLogErrorLevel()
		{
			var appenderMock = new Moq.Mock<IAppender>();
			appenderMock.Setup(a => a.Initialize()).Returns(true);

			var logger = new JobLogger(new List<IAppender> { appenderMock.Object }, LogLevel.Warning);

			var msg = "Test message";
			var msgLevel = LogLevel.Error;
			logger.LogMessage(msg, msgLevel);

			appenderMock.Verify(a => a.Log(msg, msgLevel), Times.Once);
		}


		[Test]
		public void LogMessage_ConfiguredForError_ShouldNotLogMessageLevel()
		{
			var appenderMock = new Moq.Mock<IAppender>();
			appenderMock.Setup(a => a.Initialize()).Returns(true);

			var logger = new JobLogger(new List<IAppender> { appenderMock.Object }, LogLevel.Error);

			var msg = "Test message";
			var msgLevel = LogLevel.Message;
			logger.LogMessage(msg, msgLevel);

			appenderMock.Verify(a => a.Log(msg, msgLevel), Times.Never);
		}

		[Test]
		public void LogMessage_ConfiguredForError_ShouldNotLogWarningLevel()
		{
			var appenderMock = new Moq.Mock<IAppender>();
			appenderMock.Setup(a => a.Initialize()).Returns(true);

			var logger = new JobLogger(new List<IAppender> { appenderMock.Object }, LogLevel.Error);

			var msg = "Test message";
			var msgLevel = LogLevel.Warning;
			logger.LogMessage(msg, msgLevel);

			appenderMock.Verify(a => a.Log(msg, msgLevel), Times.Never);
		}

		[Test]
		public void LogMessage_ConfiguredForError_ShouldLogErrorLevel()
		{
			var appenderMock = new Moq.Mock<IAppender>();
			appenderMock.Setup(a => a.Initialize()).Returns(true);

			var logger = new JobLogger(new List<IAppender> { appenderMock.Object }, LogLevel.Error);

			var msg = "Test message";
			var msgLevel = LogLevel.Error;
			logger.LogMessage(msg, msgLevel);

			appenderMock.Verify(a => a.Log(msg, msgLevel), Times.Once);
		}

		[Test]
		public void LogMessage_EmptyMessage_ShouldNotLogAnything()
		{
			var appenderMock = new Moq.Mock<IAppender>();
			appenderMock.Setup(a => a.Initialize()).Returns(true);

			var logger = new JobLogger(new List<IAppender> { appenderMock.Object }, LogLevel.Message);
			logger.LogMessage(string.Empty, LogLevel.Error);

			appenderMock.Verify(a => a.Log(string.Empty, LogLevel.Error), Times.Never);
		}

		[Test]
		public void LogMessage_ConfiguredWithManyAppenders_ShouldCallAllOfThem()
		{
			var appender1Mock = new Moq.Mock<IAppender>();
			appender1Mock.Setup(a => a.Initialize()).Returns(true);
			var appender2Mock = new Moq.Mock<IAppender>();
			appender2Mock.Setup(a => a.Initialize()).Returns(true);

			var msg = "test msg";
			var logger = new JobLogger(new List<IAppender> { appender1Mock.Object, appender2Mock.Object }, LogLevel.Message);
			logger.LogMessage(msg, LogLevel.Error);

			appender1Mock.Verify(a => a.Log(msg, LogLevel.Error), Times.Once);
			appender2Mock.Verify(a => a.Log(msg, LogLevel.Error), Times.Once);
		}
	}
}