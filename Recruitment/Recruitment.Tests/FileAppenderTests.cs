﻿using Moq;
using NUnit.Framework;
using Recruitment.Implementation;
using Recruitment.Intefaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Recruitment.Tests
{
	public class FileAppenderTests
	{
		[SetUp]
		public void Setup()
		{
		}

		[Test]
		public void Initialize_NoLogDirectoryInAppConfig_ShouldThrowException()
		{
			var configRepositoryMock = new Moq.Mock<IConfigRepository>();
			configRepositoryMock.Setup(c => c.GetLogFilePath()).Throws(new ArgumentException());
			var fileRepositoryMock = new Moq.Mock<IFileRepository>();

			var appender = new FileAppender(configRepositoryMock.Object, fileRepositoryMock.Object);

			Assert.Throws<ArgumentException>(() => appender.Initialize());
		}

		[Test]
		public void Initialize_LogDirectoryExsistsInAppConfig_ShouldReturnTrue()
		{
			var configRepositoryMock = new Moq.Mock<IConfigRepository>();
			configRepositoryMock.Setup(c => c.GetLogFilePath()).Returns(@"c:\directorymock");
			var fileRepositoryMock = new Moq.Mock<IFileRepository>();

			var appender = new FileAppender(configRepositoryMock.Object, fileRepositoryMock.Object);

			Assert.AreEqual(true, appender.Initialize());
		}

		[TestCase("test 1", LogLevel.Message)]
		[TestCase("test 1", LogLevel.Warning)]
		[TestCase("test 1", LogLevel.Error)]
		public void Log_CallingAllLevels_ShouldCallFileRepository(string message, LogLevel logLevel)
		{
			var configRepositoryMock = new Moq.Mock<IConfigRepository>();
			configRepositoryMock.Setup(c => c.GetLogFilePath()).Returns(@"c:\directorymock");
			var fileRepositoryMock = new Moq.Mock<IFileRepository>();

			var appender = new FileAppender(configRepositoryMock.Object, fileRepositoryMock.Object);
			appender.Initialize();
			appender.Log(message, logLevel);

			fileRepositoryMock.Verify(f => f.AppendAllText(It.IsAny<string>(), It.IsAny<string>()), Moq.Times.Once);
		}
	}
}
