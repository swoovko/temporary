﻿using Recruitment.Implementation;
using Recruitment.Intefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment.App
{
	class Program
	{
		static void Main(string[] args)
		{
			var consoleAppender = new ConsoleAppender();

			var logger = new JobLogger(new List<IAppender> { consoleAppender }, LogLevel.Message);

			logger.LogMessage("Testing message level", LogLevel.Message);
			logger.LogMessage("Testing warning level", LogLevel.Warning);
			logger.LogMessage("Testing error level", LogLevel.Error);

			Console.WriteLine("Press any key to finish...");
			Console.ReadKey();
		}
	}
}
