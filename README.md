# README #

Code review is available at (https://bitbucket.org/swoovko/temporary/pull-requests/1/messy-logger-implementation/diff)

### Solution structure ###

Solution contains 3 projects

* Recruitment - dll project, main implementation

* Recruitment.Tests - NUnit test project

* Recruitment.App - console application, example of usage
